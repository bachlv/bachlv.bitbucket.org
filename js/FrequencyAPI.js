(function(namespace){
    
    var FREQ_DEVICE_ID = "55c09f3b-e173-5597";
    var FREQ_AUTH = "ea97e6a7-5d74-4282-93c1-771027f91a99";
    var FREQ_PROD_URL = "https://prd-freq.frequency.com/api/2.0/";
    
    
    namespace.FrequencyAPI = function() {};
    namespace.FrequencyAPI.prototype = {
        generateHeaderAjaxCall: function() {
            var headers = [];
            var deviceId = {
                header: "X-Frequency-DeviceId",
                value: FREQ_DEVICE_ID
            },
            eToken = {
                header: "X-Frequency-Auth",
                value: FREQ_AUTH
            };
            headers.push(deviceId);
            headers.push(eToken);
            return headers;
        },
        getLayoutPage: function() {
            namespace.fetchData("GET", null, "json/app.json", null, this.printLayoutContent);
        },
        printLayoutContent: function(data) {
            var data = JSON.parse(data);
            (new namespace.LeftMenu()).renderLeftMenuHtml(data.pages);
            (new namespace.FrequencyAPI()).getAllCategories();
        },
        getAllCategories: function() {
            namespace.fetchData("GET", this.generateHeaderAjaxCall(), FREQ_PROD_URL + "categories", null, (new namespace.Channel()).renderCategoriesHTML);
        }
    }
    
    window.addEventListener("load", initApplication, true);
    
    function initApplication() {
        if (localStorage.getItem("prevPage")) {
            localStorage.removeItem("prevPage");
        }
        var frequencyApi = new namespace.FrequencyAPI();
        frequencyApi.getLayoutPage();
    }
    
})(window.FREQ)