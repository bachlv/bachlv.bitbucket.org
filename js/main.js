(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    window.VK_LEFT = window.VK_LEFT || 37;
	window.VK_RIGHT = window.VK_RIGHT || 39;
	window.VK_UP = window.VK_UP || 38;
	window.VK_DOWN = window.VK_DOWN || 40;
	window.VK_ENTER = window.VK_ENTER || 13;
	window.VK_BACK = window.VK_BACK || 66;
    
    var eventsArray = [];
    
    var FREQ_DEVICE_ID = "55c09f3b-e173-5597";
    var FREQ_AUTH = "ea97e6a7-5d74-4282-93c1-771027f91a99";
    var FREQ_PROD_URL = "https://prd-freq.frequency.com/api/2.0/";
/**
 * Extend object with additional object
 * @param destination Object that will be extended
 * @param source Object that will be added to destination
 */

	Object.extend = function(destination, source) {
		for (var property in source) {
			if (source.hasOwnProperty(property)) {
				destination[property] = source[property];
			}
		}
		return destination;
	};
/** 
 * Function that prepares template to later use.
 * @param template Id of a template inside index.html file to parse
 */
	Object.extend(namespace, {
		addClass: function(element, className) {
			if (!namespace.hasClass(element, className)) {
				element.className += " " + className;
			}
		},

		removeClass: function(element, className) {
			if (!element) {
				return;
			}

			element.className = element.className.replace(new RegExp(className, "g"), "").replace(/^ +| +$/g, "").replace(/ +/g, " ");
		},

		hasClass: function(element, className) {
			if (!element.className) {}
			return element && element.className && element.className.match(className);
		},

		fetchData: function(method, headers, url, data, callback, self) {
			var xhr = new XMLHttpRequest();
			xhr.open(method, url);
			xhr.setRequestHeader("Content-Type", "application/json");
            if (headers && headers.length > 0) {
                var i, leng = headers.length;
                for (i = 0; i < leng; i++) {
                    xhr.setRequestHeader(headers[i].header, headers[i].value);
                }
            }
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status == 200 && callback) {
					callback.call(self, xhr.responseText);
				} else {
                    console.log(xhr.readyState + "--" +xhr.status);
                }
			};
			xhr.send(data);
		},

        getCookie: function(key)
        {
          key=key+(namespace.VT_CONFIG.username || "");
          if (document.cookie.indexOf(key)>-1) {
            return document.cookie.match(key+"=([^;]*)")[1];
          } 
        },
        setCookie: function(key,value)
        {

          key=key+(namespace.VT_CONFIG.username || "");
          var date = new Date();
          date.setYear(2300);
          document.cookie = key + "=" + value + "; expires=" + date.toGMTString();
        },
        addEvent: function(element, eventName, functionToCall) {
            if (!eventsArray[element]) {
                eventsArray[element] = {};
            }
            if (!eventsArray[element][eventName]) {
                eventsArray[element][eventName] = [];
                element.addEventListener(eventName, function(e) {
          for (var i = eventsArray[element][eventName].length - 1; i >= 0; i--){
            if (!e.cancel) {
              var entry = eventsArray[element][eventName][i];
              entry.call(null, e);
            }
          }
                },
                true);
            }

            eventsArray[element][eventName].push(functionToCall);
        },
        removeEvent: function(element, eventName, functionToCall) {
            if (eventsArray[element] && eventsArray[element][eventName]) {
                var arr = eventsArray[element][eventName];
                var index = arr.indexOf(functionToCall);
                if (index > - 1) {
                    arr.splice(index, 1);
                }
            }
        },
        runningInBrowserOrEmulator: function(){
            var ua = navigator.userAgent;
            if (   ua.indexOf('Model') == -1
                || ua.indexOf('Model/Opera-TvEmulator') != -1
                || ua.indexOf('Model/Opera-Webkit') != -1){
                    return true;
            } else {
                return false;
            }
        },
        cssProperty: function(element, property) {
            if (element) {
                return window.getComputedStyle(element, null).getPropertyValue(property);
            } else {
                throw "Cannot read property of undefined";
            }
            
        }
        });
})(window.FREQ)