(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    var videoSection = document.getElementById("video-player");
    var playerFrame = "#player";
    var playerWrapper = videoSection.querySelector(".player-wrapper");
    var playerControl = playerWrapper.querySelector(".player-control");
    namespace.PlayerTiVo = function() {};
    namespace.PlayerTiVo.prototype = {
        convertSecondsToMinSec: function(currentTime) {
            var mins = Math.floor(currentTime / 60);
            var seconds = Math.floor(currentTime - mins * 60);
            return (mins < 10 ? '0' : '') + mins + ':' + (seconds < 10 ? '0' : '') + seconds;
        },
        loadVideo: function(movie) {
            if (movie != null) {
                var videoType = getVideoType(movie.source_url);
                console.log(videoType, "VideoType");
                var opts = {autoplay: true};
                switch (videoType) {
                    case MP4:
                        break;
                    case YOUTUBE:
                        break;
                    case HLS:
                        opts.hls = true;
                        break;
                    case VIMEO:
                        break;
                    case DAILY_MOTION:
                        break;
                    case OTHER:
                        alert("Unsupported format");
                        console.log("Unsupported format for URL", url);
                        return;
                        break;
                }
                opts.url = movie.source_url;
                window.player.loadVideo(opts);

                displayMovieInformation(movie);
            } else {
                alert("No movie response")
                console.log("No movie response");
            }
        },
        playVideo: function() {
            window.player = new Frequency.Player(
                playerFrame, {
                    session: {
                        'X-Frequency-Auth': '1111',
                        'X-Frequency-DeviceId': '22222'
                    },
                    video: {
                        videoId: 12345,
                        url: 'http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4',
                        // startPosition: 50,
                        autoplay: true,
                        // Ads configuration:
                        adsConfig: {
                            // adTagUrl: '/vast/inline_linear.xml', // VAST
                            // adTagUrl: '/vast/vmap.xml', // VAST
                            // adCuePoints: ['preroll', 12] // time, 'preroll', 'postroll'
                        }
                    },
                    events: {
                        onProgress: function(currentTime) {
                            document.querySelector('.meter').firstElementChild.style.width = (100*currentTime)/window.player.getDuration() + '%';
                            document.getElementById('current-time').innerHTML = (new namespace.PlayerTiVo()).convertSecondsToMinSec(currentTime);
                            if (parseFloat(window.player.getDuration()).toFixed(1) == window.player.getCurrentTime()) {
                                document.getElementById("play").click();
                                namespace.addClass(playerWrapper, "active");
                            }
                        },
                        onError: function(error) {
                            console.log('onError', error)
                        }
                    }
                }
            );
            if (document.getElementById("play").checked) {
                document.getElementById("play").click();
            }
        },
        seekTo: function(seconds) {
            var currentTime = window.player.getCurrentTime();
            var newTime = currentTime + seconds;
            if (newTime > window.player.getDuration()) {
                newTime = window.player.getDuration();
            } else if (newTime < 0) {
                newTime = 0;
            }
            window.player.seekTo(newTime);
        },
        fullScreenView: function() {
            videoSection.style.display = "block";
            this.playVideo();
        },
        exitFullScreen: function() {
            window.player.stop();
            videoSection.style.display = "none";
        }
    };
    document.getElementById("play").onclick = function () {
        if (this.checked) {
            window.player.pause();
            playerWrapper.classList.toggle("active");
        } else {
            window.player.play();
            playerWrapper.classList.remove("active");
        }
        pauseTrigger(this.checked);
    };
    function pauseTrigger(isAppear) {
        
    };
})(window.FREQ)