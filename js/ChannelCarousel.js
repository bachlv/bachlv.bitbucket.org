(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    
    var resultChannel = document.getElementById("list-channel-section");
    var selectedRect = document.querySelector(".select-rect");
    var rightElement = document.getElementById("right-content");
    
    namespace.Channel = function() {};
    namespace.Channel.prototype = {
        filterState: false,
        removeActiveRows: function() {
            var channelRows = this.getALLChannelRows();
            if (channelRows && channelRows.length > 0) {
                var i, leng = channelRows.length;
                for (i = 0; i < leng; i++) {
                    namespace.removeClass(channelRows[i], "active");
                }
            }
        },
        setSelectedChannelRow: function(element, itemIndex){
            if (element) {
                this.removeActiveRows();
                namespace.addClass(element, "active");
                var selectedChannel = this.getChannelItemByIndex(itemIndex);
                this.setSelectedChannelItem(selectedChannel.element, 0);
            }
        },
        setSelectedChannelRowByIndex: function(index, itemIndex) {
            var channelRows = this.getALLChannelRows();
            if (index < channelRows.length) {
                this.setSelectedChannelRow(channelRows[index], itemIndex);
            }
        },
        getSelectedChannelRow: function() {
            var result = null;
            var channelRows = this.getALLChannelRows();
            if (channelRows && channelRows.length > 0) {
                var i, leng = channelRows.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(channelRows[i], "active")) {
                        result =  {
                            index: i,
                            element: channelRows[i]
                        }
                    }
                }
                if (!result) {
                     result =  {
                            index: 0,
                            element: channelRows[0]
                        }
                }
            }
            return result;
        },
        removeSelectedItems: function() {
            var allItemInSelectedRow = this.getAllItemsInSelectedRow();
            if (allItemInSelectedRow && allItemInSelectedRow.length > 0) {
                var i, leng = allItemInSelectedRow.length;
                for (i = 0; i < leng; i++) {
                    namespace.removeClass(allItemInSelectedRow[i], "selected");
                }
            }
        },
        setSelectedChannelItem: function(element, delay) {
            if (element) {
                this.removeSelectedItems();
                namespace.addClass(element, "selected");
                this.generateRecBorder(element, delay);
            }
        },
        setSelectedChannelItemByIndex: function(index) {
            var allItems = this.getAllItemsInSelectedRow();
            if (index < allItems.length && index >= 0) {
                this.setSelectedChannelItem(allItems[index], 0);
            }
        },
        getSelectedChannelItem: function() {
            var result = null;
            var allItemInSelectedRow = this.getAllItemsInSelectedRow();
            if (allItemInSelectedRow && allItemInSelectedRow.length > 0) {
                var i, leng = allItemInSelectedRow.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(allItemInSelectedRow[i], "selected")) {
                        result = {
                            index: i,
                            element: allItemInSelectedRow[i]
                        };
                    }
                }
                if (!result) {
                    result = {
                        index: 0,
                        element: allItemInSelectedRow[0]
                    };
                }
            }
             return result;
            
        },
        getChannelItemByIndex: function(index) {
            var result = null;
            var allItemInSelectedRow = this.getAllItemsInSelectedRow();
            if (allItemInSelectedRow) {
                while (index >= allItemInSelectedRow.length) {
                     index --;
                }
                result = {
                    index: index,
                    element: allItemInSelectedRow[index]
                };
            }
            return result;
        },
        getALLChannelRows: function() {
            return resultChannel.children;
        },
        getAllItemsInSelectedRow: function() {
            var selectedRow = this.getSelectedChannelRow();
            if (selectedRow) {
                return selectedRow.element.children;
            } else {
                return undefined;
            }
        },
        isLastChannelRow: function(index) {
            var allRows = this.getALLChannelRows();
            if (allRows && index >= allRows.length) {
                return true;
            } else {
                return false;
            }
        },
        generateRecBorder: function (element, delay) {
            //TODO
            var timeout = delay;
            selectedRect.style.display = 'none';
            var position = element.getBoundingClientRect();
            if (position.top < 95 || (position.top + position.height) > 600) {
                element.parentElement.scrollIntoView({
                    block: "start",
                    behavior: "smooth"
                });
            }
            setTimeout(function () {
                position = element.getBoundingClientRect();
                if (selectedRect) {
                    selectedRect.style.height = (position.height - 5) + "px";
                    selectedRect.style.width = (position.width - 5) + "px";
                    selectedRect.style.top = (resultChannel.scrollTop + position.top - 95) + "px";
                    selectedRect.style.left = (position.left) + "px";
                    selectedRect.style.display = 'block';
                }
            }, timeout);

        },
        showForm: function() {
            selectedRect.style.display = 'none';
            document.getElementById("left-navigation").style.display = "none";
            document.querySelector(".left-background").style.display = "none";
            rightElement.style.paddingLeft = "0px";
            rightElement.querySelector(".first-level").style.display = "none";
            rightElement.querySelector(".second-level").style.display = "block";
            document.querySelector(".dropdown-group").scrollIntoView();
            var selectedChannel = this.getSelectedChannelItem();
            this.setSelectedChannelItem(selectedChannel.element, 500);
        },
        hiddenChannelContent: function() {
            document.getElementById("left-navigation").style.display = "block";
            document.querySelector(".left-background").style.display = "block";
            rightElement.style.paddingLeft = "120px";
            rightElement.querySelector(".first-level").style.display = "block";
            rightElement.querySelector(".second-level").style.display = "none";
        },
        processKeyEvent: function(keyCode) {
            var currentSelectedRow = this.getSelectedChannelRow();
            var currentSelectedItem = this.getSelectedChannelItem();
            switch (keyCode) {
                case window.VK_BACK:
                    this.hiddenChannelContent();
                    break;
                case window.VK_DOWN:
                    var newIndex = currentSelectedRow.index + 1;
                    if (this.filterState) {
                        newIndex = currentSelectedRow.index;
                        this.filterState = false;
                    }
                    this.setSelectedChannelRowByIndex(newIndex, currentSelectedItem.index);
                    break;
                case window.VK_ENTER:
                    (new namespace.Video()).showForm();
                    break;
                case window.VK_LEFT:
                    var newIndex = currentSelectedItem.index - 1;
                    this.setSelectedChannelItemByIndex(newIndex);
                    break;
                case window.VK_RIGHT:
                    var newIndex = currentSelectedItem.index + 1;
                    this.setSelectedChannelItemByIndex(newIndex);
                    break;
                case window.VK_UP:
                    var newIndex = currentSelectedRow.index - 1;
                    if (newIndex >= 0) {
                        this.setSelectedChannelRowByIndex(newIndex, currentSelectedItem.index);
                    } else {
                        document.querySelector(".dropdown-group").scrollIntoView();
                        selectedRect.style.display = 'none';
                        this.removeSelectedItems();
                        this.removeActiveRows();
                        this.filterState = true;
                    }
                    break;
            }
        },
        renderCategoriesHTML: function(data) {
            console.log(data);
        }
    }
})(window.FREQ)