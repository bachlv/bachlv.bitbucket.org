(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    var dialog = document.getElementsByTagName("dialog")[0];
    
    namespace.Dialog = function() {};
    namespace.Dialog.prototype = {
        message: "",
        contructor: function(message){
            this.message = message;
        },
        setContent: function() {
            
        },
        openDialog: function() {
            var att = document.createAttribute("open");
            dialog.setAttributeNode(att)
        },
        closeDialog: function() {
            dialog.removeAttribute("open");
        },
        isShow: function() {
            return dialog.hasAttribute("open");
        }
    };
})(window.FREQ)