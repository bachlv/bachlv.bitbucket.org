(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    * LeftMenu.js is file for all of function are execute on Left Navigation
    */
    
    var leftMenu = document.getElementById("left-navigation");
    var leftBackground = document.querySelector(".left-background");
    
    var templateHtml = '<div id="" class=""><img src="" width="32" height="32" alt=""><div class="image-text">Discover</div></div>';
    
    namespace.LeftMenu = function() {};
    namespace.LeftMenu.prototype = {
        /*List all menu item id of list Menu*/
        MENU_PAGE: ['menu-discover','menu-favorite', 'menu-channel', 'menu-search'],
        /*check if left menu is collapse or expand*/
        isActive: function() {
            if (leftMenu.getAttribute("class").indexOf("menu-min") == -1) {
                return true;
            } else {
                return false;
            }
            
        },
        removeActiveItem: function(element) {
            namespace.removeClass(element, "active");
        },
        /* set new selected menu item when catch event keyUp and keyDown*/
        setNewSelectedItem: function(keyCode) {
            var currentSelect = this.getCurrentSelectedItem();
            if (currentSelect != undefined) {
                this.removeActiveItem(currentSelect.item);
                var newIndex;
                if (keyCode == window.VK_UP) {
                    newIndex = currentSelect.index - 1;
                    if ((currentSelect.index - 1) < 0) {
                        newIndex = this.getNumberOfItem() - 1;
                    }
                } else if (keyCode == window.VK_DOWN) {
                    newIndex = currentSelect.index + 1;
                    if ((currentSelect.index + 1) >= this.getNumberOfItem()) {
                        newIndex = 0;
                    }
                }
                var listItem = this.getAllMenuItems();
                if (listItem) {
                    namespace.addClass(listItem[newIndex], "active");
                }
                this.updateRightContent();
            }
        }
        ,
        /* find element and index of selected menu item*/
        getCurrentSelectedItem: function() {
            var listItem = this.getAllMenuItems();
            if (listItem && this.getNumberOfItem() > 0) {
                var i;
                var leng = this.getNumberOfItem();
                for (i = 0; i < leng; i++) {
                    if (listItem[i].className.indexOf("active") > -1 ) {
                        return {
                            index: i,
                            item: listItem[i]
                        };
                    }
                }
            } else {
                return undefined;
            }
        },
        /* Get All of Element in Left Navigation */
        getAllMenuItems: function() {
            return leftMenu.children;
        },
        /* Get Length of all items in Left Navigation */
        getNumberOfItem: function() {
            return leftMenu.children.length;
        },
        /* Execute collapse Left Navigation when focus on Right Content */
        collapseMenu: function() {
            namespace.addClass(leftMenu, "menu-min");
            leftMenu.style.width = "80px";
            leftBackground.style.width = "80px";
        },
        /* Execute expand Left Navigation when focus on Right Content */
        expandMenu: function() {
            namespace.removeClass(leftMenu, "menu-min");
            leftMenu.style.width = "220px";
            leftBackground.style.width = "220px";
        },
        /* Get Current Menu Item title use for Right content to change content*/
        getCurrentPage: function() {
            var currentSelect = this.getCurrentSelectedItem();
            if (currentSelect != undefined) {
                var element = currentSelect.item;
                var i;
                for (i = 0; i < this.MENU_PAGE.length; i ++) {
                    if (element.getAttribute("id") == this.MENU_PAGE[i]) {
                        return this.MENU_PAGE[i];
                    }
                }
            } else {
                return null;
            }
        },
        updateRightContent: function() {
            var textTittle = this.getCurrentSelectedItem().item.querySelector(".image-text").innerHTML;
            document.querySelector(".seperate").innerHTML = textTittle;
            (new namespace.RightContent()).updateContentByLeftMenu();
        },
        renderLeftMenuHtml: function(data) {
            if (data && data.length > 0) {
                leftMenu.innerHTML = "";
                var i, leng = data.length;
                for (i = 0; i < leng; i++) {
                    var divElement = document.createElement("div");
                    var imageElement = document.createElement("img");
                    var titleElement = document.createElement("div");
                    divElement.setAttribute("id", "menu-" + data[i].pageId.toLowerCase());
                    divElement.setAttribute("class", "image-button");
                    if (i == 0) {
                        divElement.classList.add("active");
                    }
                    imageElement.setAttribute("src", data[i].icon);
                    imageElement.setAttribute("width", "32");
                    imageElement.setAttribute("height", "32");
                    
                    titleElement.setAttribute("class", "image-text");
                    titleElement.innerHTML = data[i].title;
                    
                    divElement.appendChild(imageElement);
                    divElement.appendChild(titleElement);
                    leftMenu.appendChild(divElement);
                }
            }
        }
    }
    
})(window.FREQ)