(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    var resultVideo = document.getElementById("list-video-section");
    var selectedRect = document.querySelector(".select-rect");
    var rightElement = document.getElementById("right-content");
    var playerTivo = new namespace.PlayerTiVo();
    
    namespace.Video = function() {};
    namespace.Video.prototype = {
        removeActiveRows: function() {
            var videoRows = this.getALLVideoRows();
            if (videoRows && videoRows.length > 0) {
                var i, leng = videoRows.length;
                for (i = 0; i < leng; i++) {
                    namespace.removeClass(videoRows[i], "active");
                }
            }
        },
        setSelectedVideoRow: function(element, itemIndex){
            if (element) {
                this.removeActiveRows();
                namespace.addClass(element, "active");
                var selectedVideo = this.getVideoItemByIndex(itemIndex);
                this.setSelectedVideoItem(selectedVideo.element, 0);
            }
        },
        setSelectedVideoRowByIndex: function(index, itemIndex) {
            var videoRows = this.getALLVideoRows();
            if (index < videoRows.length) {
                this.setSelectedVideoRow(videoRows[index], itemIndex);
            }
        },
        getSelectedVideoRow: function() {
            var result = null;
            var videoRows = this.getALLVideoRows();
            if (videoRows && videoRows.length > 0) {
                var i, leng = videoRows.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(videoRows[i], "active")) {
                        result =  {
                            index: i,
                            element: videoRows[i]
                        }
                    }
                }
                if (!result) {
                     result =  {
                            index: 0,
                            element: videoRows[0]
                        }
                }
            }
            return result;
        },
        removeSelectedItems: function() {
            var allItemInSelectedRow = this.getAllItemsInSelectedRow();
            if (allItemInSelectedRow && allItemInSelectedRow.length > 0) {
                var i, leng = allItemInSelectedRow.length;
                for (i = 0; i < leng; i++) {
                    namespace.removeClass(allItemInSelectedRow[i], "selected");
                }
            }
        },
        setSelectedVideoItem: function(element, delay) {
            if (element) {
                this.removeSelectedItems();
                namespace.addClass(element, "selected");
                this.generateRecBorder(element, delay);
            }
        },
        setSelectedVideoItemByIndex: function(index) {
            var allItems = this.getAllItemsInSelectedRow();
            if (index < allItems.length && index >= 0) {
                this.setSelectedVideoItem(allItems[index], 0);
            }
        },
        getSelectedVideoItem: function() {
            var result = null;
            var allItemInSelectedRow = this.getAllItemsInSelectedRow();
            if (allItemInSelectedRow && allItemInSelectedRow.length > 0) {
                var i, leng = allItemInSelectedRow.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(allItemInSelectedRow[i], "selected")) {
                        result = {
                            index: i,
                            element: allItemInSelectedRow[i]
                        };
                    }
                }
                if (!result) {
                    result = {
                        index: 0,
                        element: allItemInSelectedRow[0]
                    };
                }
            }
             return result;
            
        },
        getVideoItemByIndex: function(index) {
            var result = null;
            var allItemInSelectedRow = this.getAllItemsInSelectedRow();
            if (allItemInSelectedRow) {
                while (index >= allItemInSelectedRow.length) {
                     index --;
                }
                result = {
                    index: index,
                    element: allItemInSelectedRow[index]
                };
            }
            return result;
        },
        getALLVideoRows: function() {
            return resultVideo.children;
        },
        getAllItemsInSelectedRow: function() {
            var selectedRow = this.getSelectedVideoRow();
            if (selectedRow) {
                return selectedRow.element.children;
            } else {
                return undefined;
            }
        },
        isLastVideoRow: function(index) {
            var allRows = this.getALLChannelRows();
            if (allRows && index >= allRows.length) {
                return true;
            } else {
                return false;
            }
        },
        generateRecBorder: function (element, delay) {
            //TODO
            var timeout = delay;
            selectedRect.style.display = 'none';
            var position = element.getBoundingClientRect();
            if (position.top < 200 || (position.top + position.height) > 600) {
                element.parentElement.scrollIntoView({
                    block: "start",
                    behavior: "smooth"
                });
            }
            setTimeout(function () {
                position = element.getBoundingClientRect();
                if (selectedRect) {
                    selectedRect.style.height = (position.height - 5) + "px";
                    selectedRect.style.width = (position.width - 5) + "px";
                    selectedRect.style.top = (resultVideo.scrollTop + position.top - 95) + "px";
                    selectedRect.style.left = (position.left) + "px";
                    selectedRect.style.display = 'block';
                }
            }, timeout);

        },
        showForm: function() {
            selectedRect.style.display = 'none';
            document.getElementById("left-navigation").style.display = "none";
            document.querySelector(".left-background").style.display = "none";
            rightElement.style.paddingLeft = "0px";
            rightElement.querySelector(".first-level").style.display = "none";
            rightElement.querySelector(".second-level").style.display = "block";
            document.getElementById("filter-channel").style.display = "none";
            document.getElementById("detail-channel").style.display = "block";
            var selectedVideo = this.getSelectedVideoItem();
            this.setSelectedVideoItem(selectedVideo.element, 500);
        },
        processKeyEvent: function(keyCode) {
            var currentSelectedRow = this.getSelectedVideoRow();
            var currentSelectedItem = this.getSelectedVideoItem();
            switch (keyCode) {
                case window.VK_DOWN:
                    var newIndex = currentSelectedRow.index + 1;
                    this.setSelectedVideoRowByIndex(newIndex, currentSelectedItem.index);
                    break;
                case window.VK_ENTER:
                    playerTivo.fullScreenView();
                    break;
                case window.VK_LEFT:
                    var newIndex = currentSelectedItem.index - 1;
                    this.setSelectedVideoItemByIndex(newIndex);
                    break;
                case window.VK_RIGHT:
                    var newIndex = currentSelectedItem.index + 1;
                    this.setSelectedVideoItemByIndex(newIndex);
                    break;
                case window.VK_UP:
                    var newIndex = currentSelectedRow.index - 1;
                    if (newIndex >= 0) {
                        this.setSelectedVideoRowByIndex(newIndex, currentSelectedItem.index);
                    }
                    break;
            }
        }
    }
})(window.FREQ)