(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    namespace.addEvent(document, 'keydown', keyBoardEvenHandler);
    var leftMenu = new namespace.LeftMenu();
    var rightContent = new namespace.RightContent();
    function keyBoardEvenHandler(evt) {
        switch (evt.keyCode) {
            case window.VK_RIGHT:
                if(leftMenu.isActive()) {
                    leftMenu.collapseMenu();
                    rightContent.expandContent();
                    setTimeout(function() {
                        rightContent.processKeyBoardEvent(window.VK_RIGHT);
                    }, 600);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_RIGHT);
                }
                
                break;
            case window.VK_LEFT:
                if(!leftMenu.isActive()) {
                    rightContent.processKeyBoardEvent(window.VK_LEFT);
                } 
                break;
            case window.VK_UP:
                if(leftMenu.isActive()) {
                    leftMenu.setNewSelectedItem(window.VK_UP);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_UP);
                }
                break;
            case window.VK_DOWN:
                if(leftMenu.isActive()) {
                    leftMenu.setNewSelectedItem(window.VK_DOWN);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_DOWN);
                }
                break;
            case window.VK_ENTER:
                if(leftMenu.isActive()) {
                    leftMenu.collapseMenu();
                    rightContent.expandContent();
                    setTimeout(function() {
                        rightContent.processKeyBoardEvent(window.VK_RIGHT);
                    }, 600);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_ENTER);
                }
                break;
            case window.VK_BACK:
                if(leftMenu.isActive()) {
                    window.close();
                } else {
                    rightContent.processKeyBoardEvent(window.VK_BACK);
                }
                break;
        }
        evt.preventDefault();
    }
})(window.FREQ)