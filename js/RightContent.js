(function (namespace) {

    /*
     * Author: LongLV
     * CreateDate: 24/08/2016
     * RightContent.js is file for all of function are execute on Left Navigation
     */


    var rightElement = document.getElementById("right-content");
    var firstLevelContent = rightElement.querySelector(".first-level");
    var selectedRect = document.querySelector(".select-rect");
    var dialog = new namespace.Dialog();
    var leftMenu = new namespace.LeftMenu();
    var channelController = new namespace.Channel();
    var videoController = new namespace.Video();
    var playerTivo = new namespace.PlayerTiVo();

    namespace.RightContent = function () {};
    namespace.RightContent.prototype = {
        /*List all page content base on left navigation*/
        DISCOVER_PAGE: 'menu-discover',
        FAVORITE_PAGE: 'menu-favorite',
        CHANNELS_PAGE: 'menu-channel',
        SEARCH_PAGE: 'menu-search',
        MENU_STATUS: true,
        CONTENT_TYPE: ["category", "channel", "video"],
        isLastRow: function (index) {
            var listRow = this.getAllRowOfVisibleContent();
            return index > (listRow.length - 1);
        },
        removeClassSelectedRow: function () {
            var listRow = this.getAllRowOfVisibleContent();
            if (listRow && listRow.length > 0) {
                var i, leng = listRow.length;
                for (i = 0; i < leng; i++) {
                    namespace.removeClass(listRow[i], 'active');
                }
            }
        },
        getSelectedRow: function () {
            var listRow = this.getAllRowOfVisibleContent();
            var selectedRow = null;
            if (listRow && listRow.length > 0) {
                var i, leng = listRow.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(listRow[i], 'active')) {
                        selectedRow = {
                            index: i,
                            element: listRow[i]
                        };
                        break;
                    }
                }
                if (selectedRow == null) {
                    selectedRow = {
                        index: 0,
                        element: listRow[0]
                    };
                }
            }
            return selectedRow;
        },
        setSelectedRow: function (index) {
            this.removeClassSelectedRow();
            var listRow = this.getAllRowOfVisibleContent();
            if (listRow && index < listRow.length) {
                namespace.addClass(listRow[index], "active");
                this.setSelectedCarouseItem(this.getSelectedCarouseItem().index);
            }
        },
        isLastCarouselItem: function (index, row) {
            return index > (row.length - 1);
        },
        removeClassSelectedCarouselItem: function () {
            var listCarousel = this.getSelectedListCarousel();
            if (listCarousel && listCarousel.length > 0) {
                var i, leng = listCarousel.length;
                for (i = 0; i < leng; i++) {
                    namespace.removeClass(listCarousel[i], 'selected');
                }
            }
        },
        getSelectedListCarousel: function () {
            var selectedRow = this.getSelectedRow().element;
            return selectedRow.querySelector(".carousel-container").children;
        },
        getSelectedCarouseItem: function () {
            var selectedCarouselItem = null;
            var listCarousel = this.getSelectedListCarousel();
            if (listCarousel && listCarousel.length > 0) {
                var i, leng = listCarousel.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(listCarousel[i], 'selected')) {
                        selectedCarouselItem = {
                            index: i,
                            element: listCarousel[i],
                            type: this.getTypeOfItem(listCarousel[i].parentElement)
                        };
                    }
                }
                if (selectedCarouselItem == null) {
                    selectedCarouselItem = {
                        index: 0,
                        element: listCarousel[0],
                        type: this.getTypeOfItem(listCarousel[0].parentElement)
                    };
                }
            }
            return selectedCarouselItem;

        },
        setSelectedCarouseItem: function (index) {
            var listCarousel = this.getSelectedListCarousel();
            this.removeClassSelectedCarouselItem();
            if (listCarousel && index < listCarousel.length) {
                namespace.addClass(listCarousel[index], "selected");
                this.generateRecBorder(listCarousel[index]);
            }
        },
        getTypeOfItem: function (element) {
            var result = "";
            for (var index in this.CONTENT_TYPE) {
                if (namespace.hasClass(element, this.CONTENT_TYPE[index])) {
                    result = this.CONTENT_TYPE[index];
                }
            }
            return result;
        },
        generateRecBorder: function (element) {
            //TODO
            var timeout = 0;
            selectedRect.style.display = 'none';
            var position = element.getBoundingClientRect();
            if (position.top < 95 || (position.top + position.height) > 600) {
                element.parentElement.parentElement.scrollIntoView({
                    block: "start",
                    behavior: "smooth"
                });
            }
            var leftPosition = namespace.cssProperty(element.parentElement, "left");
            if (leftPosition == "auto") {
                leftPosition = element.parentElement.offsetLeft;
            } else {
                leftPosition = parseInt(leftPosition.substring(0, (leftPosition.length - 2)));
            }
            if (position.right > 1280) {
                element.parentElement.style.left = (leftPosition - position.width - 25) + "px";
                timeout = 300;
            }
            if (position.left < 130) {
                element.parentElement.style.left = (leftPosition + position.width + 25) + "px";
                timeout = 300;
            }
            setTimeout(function () {
                position = element.getBoundingClientRect();
                if (selectedRect && !leftMenu.isActive()) {
                    selectedRect.style.height = (position.height - 5) + "px";
                    selectedRect.style.width = (position.width - 5) + "px";
                    selectedRect.style.top = (rightElement.scrollTop + position.top - 95) + "px";
                    selectedRect.style.left = (position.left) + "px";
                    selectedRect.style.display = 'block';
                }
            }, timeout);

        },
        resetPosition: function () {
            var listRow = this.getAllRowOfVisibleContent();
            var selectedRow = null;
            if (listRow && listRow.length > 0) {
                var i, leng = listRow.length;
                for (i = 0; i < leng; i++) {
                    listRow[i].querySelector(".carousel-container").style.left = "40px";
                    namespace.removeClass(listRow[i], "active");
                }
            }
        },
        expandContent: function () {
            rightElement.style.paddingLeft = "120px";
        },
        collapseContent: function () {
            rightElement.style.paddingLeft = "270px";
        },
        getAllPagesInRightContent: function () {
            if (firstLevelContent.children && firstLevelContent.children.length > 0) {
                return firstLevelContent.children;
            } else {
                throw "Cannot get all pages in right content";
            }
        },
        getContentVisible: function () {
            var allPageContent = this.getAllPagesInRightContent();
            if (allPageContent) {
                var i, leng = allPageContent.length;
                for (i = 0; i < leng; i++) {
                    if (window.getComputedStyle(allPageContent[i]).display != 'none') {
                        return allPageContent[i];
                    }
                }
            } else {
                console.error("Cannot find visible element!");
                return null;
            }

        },
        getAllRowOfVisibleContent: function () {
            var visiblePage = this.getContentVisible();
            if (visiblePage) {
                var allRow = visiblePage.children;
                var result = [];
                if (allRow) {
                    var i, leng = allRow.length;
                    for (i = 0; i < leng; i++) {
                        if (allRow[i].tagName == 'DIV') {
                            result.push(allRow[i]);
                        }
                    }
                }
                return result;
            } else {
                return null;
            }
        },
        isSecondScreen: function() {
            return (window.getComputedStyle(document.querySelector(".second-level"), null).display != "none");
        },
        isChannelFilterPage: function() {
            return (window.getComputedStyle(document.getElementById("filter-channel"), null).display != "none");
        },
        isChannelDetailPage: function() {
            return (window.getComputedStyle(document.getElementById("detail-channel"), null).display != "none");
        },
        isFullScreenView: function() {
            return (window.getComputedStyle(document.getElementById("video-player"), null).display != "none");
        },
        hiddenAllContent: function () {
            try {
                var allPageContent = this.getAllPagesInRightContent();
                var leng = allPageContent.length;
                for (var i = 0; i < leng; i++) {
                    allPageContent[i].style.display = 'none';
                }
            } catch (e) {
                console(e);
            }
        },
        updateContentByLeftMenu: function () {
            var contentPage = leftMenu.getCurrentPage();
            this.hiddenAllContent();
            switch (contentPage) {
                case this.DISCOVER_PAGE:
                    firstLevelContent.querySelector("#discover-page").style.display = 'block';
                    break;
                case this.FAVORITE_PAGE:
                    firstLevelContent.querySelector("#favorite-page").style.display = 'block';
                    break;
                case this.CHANNELS_PAGE:
                    firstLevelContent.querySelector("#channel-page").style.display = 'block';
                    break;
                case this.SEARCH_PAGE:
                    firstLevelContent.querySelector("#search-page").style.display = 'block';
                    break;
            }
        },
        updateContenByChannel: function (selectedCarouselItem) {
            switch (selectedCarouselItem.type) {
                case this.CONTENT_TYPE[0]:
                    localStorage.setItem("prevPage", "category");
                    channelController.showForm();
                    break;
                case this.CONTENT_TYPE[1]:
                    localStorage.setItem("prevPage", "channel");
                    (new namespace.Video()).showForm();
                    break;
                case this.CONTENT_TYPE[2]:
                    playerTivo.fullScreenView();
                    break;
            }
        },
        processLeftKey: function(index) {
            if (!this.isSecondScreen()) {
                var newIndex = index - 1;
                if (newIndex >= 0) {
                    this.setSelectedCarouseItem(newIndex);
                } else {
                    leftMenu.expandMenu();
                    this.collapseContent();
                    this.MENU_STATUS = true;
                    this.resetPosition();
                    selectedRect.style.display = 'none';
                }
            } else {
                if (this.isChannelFilterPage()) {
                    channelController.processKeyEvent(window.VK_LEFT);
                }
                if (this.isChannelDetailPage()) {
                    videoController.processKeyEvent(window.VK_LEFT);
                }
            }
        },
        processRightKey: function(rowIndex, itemIndex) {
            if (!this.isSecondScreen()) {
                if (this.MENU_STATUS) {
                    this.setSelectedRow(rowIndex);
                    this.MENU_STATUS = false;
                } else {
                    var newIndex = itemIndex + 1;
                    if (!this.isLastCarouselItem(newIndex, this.getSelectedListCarousel())) {
                        this.setSelectedCarouseItem(newIndex);
                    }
                }
            } else {
                if (this.isChannelFilterPage()) {
                    channelController.processKeyEvent(window.VK_RIGHT);
                }
                if (this.isChannelDetailPage()) {
                    videoController.processKeyEvent(window.VK_RIGHT);
                }
            }
            
        },
        processUpKey: function(index) {
            if(!this.isSecondScreen()) {
                var newIndex = index - 1;
                if (newIndex >= 0) {
                    this.setSelectedRow(newIndex);
                }
            } else {
               if (this.isChannelFilterPage()) {
                    channelController.processKeyEvent(window.VK_UP);
                }
                if (this.isChannelDetailPage()) {
                    videoController.processKeyEvent(window.VK_UP);
                }
            }
            
        },
        processDownKey: function(index) {
            if(!this.isSecondScreen()) {
                var newIndex = index + 1;
                if (!this.isLastRow(newIndex)) {
                    this.setSelectedRow(newIndex);
                }
            } else {
               if (this.isChannelFilterPage()) {
                    channelController.processKeyEvent(window.VK_DOWN);
                }
                if (this.isChannelDetailPage()) {
                    videoController.processKeyEvent(window.VK_DOWN);
                }
            }
        },
        processEnterKey: function(selectedCarouselItem) {
            if(!this.isSecondScreen()) {
                //dialog.openDialog();
                this.updateContenByChannel(selectedCarouselItem);
            } else {
                if (this.isChannelFilterPage()) {
                    channelController.processKeyEvent(window.VK_ENTER);
                    return true;
                }
                if (this.isChannelDetailPage()) {
                    videoController.processKeyEvent(window.VK_ENTER);
                    return true;
                }
            }
        },
        processBackKey: function(index) {
            if (dialog.isShow()) {
                dialog.closeDialog();
                selectedRect.style.display = "block";
                return true;
            }
            if (this.isFullScreenView()) {
                playerTivo.exitFullScreen();
                return true;
            }
            if (this.isSecondScreen()) {
                selectedRect.style.display = "none";
                if (this.isChannelFilterPage()) {
                    channelController.processKeyEvent(window.VK_BACK);
                    setTimeout(function() {
                        (new namespace.RightContent()).setSelectedCarouseItem(index);
                    }, 500);
                }
                if (this.isChannelDetailPage()) {
                    if (localStorage.getItem("prevPage") == "channel") {
                        channelController.processKeyEvent(window.VK_BACK);
                        setTimeout(function() {
                            (new namespace.RightContent()).setSelectedCarouseItem(index);
                        }, 500);
                    }
                    document.getElementById("detail-channel").style.display = "none";
                    document.getElementById("filter-channel").style.display = "block";
                    
                    if (localStorage.getItem("prevPage") == "category") {
                        channelController.showForm();
                    }
                }
                return true;
            }
        },
        processKeyBoardEvent: function (keyCode) {
            var rowSelected = this.getSelectedRow();
            var selectedCarouselItem = this.getSelectedCarouseItem();
            var dialog = new namespace.Dialog();
            switch (keyCode) {
                case window.VK_UP:
                    this.processUpKey(rowSelected.index);
                    break;
                case window.VK_DOWN:
                    this.processDownKey(rowSelected.index);
                    break;
                case window.VK_LEFT:
                    this.processLeftKey(selectedCarouselItem.index);
                    break;
                case window.VK_RIGHT:
                    this.processRightKey(rowSelected.index, selectedCarouselItem.index);
                    break;
                case window.VK_ENTER:
                    this.processEnterKey(selectedCarouselItem);
                    break;
                case window.VK_BACK:
                    this.processBackKey(selectedCarouselItem.index);
                    break;
            }
        },
        renderRightContentHtml: function() {
            
        }
    }

})(window.FREQ)